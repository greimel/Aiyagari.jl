# Aiyagari.jl

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://greimel.gitlab.io/Aiyagari.jl/dev)
[![Build Status](https://gitlab.com/greimel/Aiyagari.jl/badges/master/pipeline.svg)](https://gitlab.com/greimel/Aiyagari.jl/-/commits/master)
[![Coverage](https://gitlab.com/greimel/Aiyagari.jl/badges/master/coverage.svg)](https://gitlab.com/greimel/Aiyagari.jl/commits/master)


This package is registered in my own registry. For installing the package, enter package-mode (type `]` in the julia REPL) and then

```julia
pkg> registry add https://github.com/JuliaRegistries/General
pkg> registry add https://gitlab.com/greimel/GreimelRegistry
pkg> add Aiyagari
```
in the julia REPL. See the documentation for how to solve macroeconomic models using the package.

For running the tests locally, run

```julia
pkg> test Aiyagari
```

For now, the package can solve the Huggett model and multiple version of macroeconomic models with infinitely-lived heterogenous agents with housing.

* only homeowners (with or without adjustment costs)
* homeowners and renters with size-separated markets properties to be rented and owned
