# # Adding more states

using Revise #src
using Test #jl
using Housing, DisposableIncomes
using QuantEcon, Parameters
using Plots #md

PKG_HOME = joinpath(dirname(pathof(Housing.Aiyagari)), "..")

# ## Enlarging the state space

# First, let's add an additional exogenous state variable

z_grid = log.([0.5; 1.0; 1.5])
z_prob = [0.7 0.15 0.15;
          0.2 0.6 0.2;
          0.15 0.15 0.7]
z_MC = MarkovChain(z_prob, z_grid, :y)

x_grid = [1, 2, 3]
x_prob = [0.7 0.3 0.0;
          0.0 0.99 0.01;
          1.0 0.0 0.0]
          
x_MC = MarkovChain(x_prob, x_grid, :x)

exo1 = ExogenousStateSpace([z_MC])
exo2 = ExogenousStateSpace([z_MC, x_MC])

# Second, let's also add an additional endogenous state variable

w_grid = LinRange(0.0, 1.0, 40)
h_grid = LinRange(0.0, 2.6, 15)

endo1 = EndogenousStateSpace((w=w_grid, ))
endo2 = EndogenousStateSpace((w=w_grid, h=h_grid))

# ## Solve the model with and without redundant states

r = 0.05
p = 0.9
param  = (β = 0.7, θ = 0.9, η = 1, δ = 0.1, ξ = 0.8159, h_thres = eps(), tt=NoGovernment(), inc=SimpleGrossIncomes(z_MC))
agg_state = (r=r, p=p)

@time out11 = solve_bellman(endo1, exo1, agg_state, param, Owner(state = OneState()), rtol=√eps())
#0.43 s (49 it)
@time out21 = solve_bellman(endo2, exo1, agg_state, param, Owner(state = WealthHouse()), rtol=√eps())
#4.85 s (49 it)
@time out12 = solve_bellman(endo1, exo2, agg_state, param, Owner(state = OneState()), rtol=√eps())
#1.03 s (49 it)
@time out22 = solve_bellman(endo2, exo2, agg_state, param, Owner(state = WealthHouse()), rtol=√eps())
#14.41 s (49 it)

dist11 = stationary_distribution(endo1, exo1, out11.policies_full.next_state)
dist21 = stationary_distribution(endo2, exo1, out21.policies_full.next_state)
dist12 = stationary_distribution(endo1, exo2, out12.policies_full.next_state)
dist22 = stationary_distribution(endo2, exo2, out22.policies_full.next_state)

# Check if results are the same
val11 = out11.val
plt11 = plot(w_grid, val11, alpha=0.35, title="no redundant states") #md

plt21 = plot!(plt11, legend=false, title="redundant endogenous state") #md
@testset "irrelevant endogenous state" begin #jl
  val21 = reshape(out21.val, (size(endo2)..., length(exo1)))
  let #jl
    max_diff = Inf #jl
    for i_h in 1:length(h_grid)
      val_i = val21[:,i_h,:]
      plot!(plt21, w_grid, val_i, alpha = 0.35 / length(h_grid)) #md
      @test isapprox(val11, val_i, rtol = eps()^(1/3)) #jl
      max_i = maximum(abs, val11 .- val_i) #jl
      max_diff = max_diff < max_i ? max_diff : max_i  #jl
     end
    println(max_diff) #jl
  end #jl
end #jl


@testset "irrelevant exogenous state" begin #jl
  val12 = reshape(out12.val, (length(endo1), size(exo2)...)) #jl
  let #jl
    max_diff = Inf #jl
    for i_x in 1:length(x_grid) #jl
      val_i = val12[:,:,i_x] #jl
      @test isapprox(val11, val_i, rtol = eps()^(1/3)) #jl
      max_i = maximum(abs, val11 .- val_i) #jl
      max_diff = max_diff < max_i ? max_diff : max_i  #jl
    end #jl
    println(max_diff) #jl
  end #jl
end #jl
plt12 = plot!(plt11, w_grid, out12.val, alpha=0.35, legend=false,  title="redundant exogenous state") #md

plt22 = plot!(plt11, legend=false, title="redundant endo + exo state")  #md
@testset "irrelevant endogenous and exogenous states" begin #jl
   val22 = reshape(out22.val, (size(endo2)..., size(exo2)...))
   let #jl
     max_diff = Inf #jl
     for i_x in 1:length(x_grid)
       for i_h in 1:length(h_grid)
        val_i = val22[:,i_h,:,i_x]
        plot!(plt22, w_grid, val_i, alpha = 0.35 / length(x_grid) / length(h_grid)) #md
        @test isapprox(val11, val_i, rtol = eps()^(1/3)) #jl
        max_i = maximum(abs, val11 .- val_i) #jl
        max_diff = max_diff < max_i ? max_diff : max_i  #jl
      end
    end
    println(max_diff) #jl
  end #jl
end #jl

display(plt11) #md
#-
display(plt12) #md
#-
display(plt21) #md
#-
display(plt22) #md
#plot(plt11, plt12, plt21, plt22) #md

@testset "stationary distribution with redundant states" begin #jl
∑dist21 = dropdims(sum(reshape(dist21, (size(endo2)..., length(exo1))), dims=2), dims=2)
@test maximum(abs, ∑dist21 .- dist11) < 2e-6 #jl

∑dist12 = dropdims(sum(reshape(dist12, (length(endo1), size(exo2)...)), dims=3), dims=3)
@test maximum(abs, ∑dist12 .- dist11) < 2e-6 #jl

∑dist22 = dropdims(sum(reshape(dist22, (size(endo2)..., size(exo2)...)), dims=[2,4]), dims=(2,4))
@test maximum(abs, ∑dist22 .- dist11) < 2e-6 #jl
end #jl

plot(w_grid, dist11, legend=false) #md
plot!(w_grid, ∑dist21) #md
plot!(w_grid, ∑dist12) #md
plot!(w_grid, ∑dist22) #md
