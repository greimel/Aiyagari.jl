# ## Define the household's problem in the Huggett model

u(c) = c > 0 ? log(c) : 10000 * c - 10000 * one(c)

function get_c(choices, states, params)
  @unpack a_next = choices
  @unpack exo_state, endo_state = states
  @unpack a = endo_state
  @unpack r = exo_state
  
  inc = disposable_income(params.inc, params.tt, exo_state)
  c = a + inc - a_next/(1+r)
end

function objective(choices, states, 𝔼V, params)
  @unpack a_next = choices
  @unpack β = params
  
  c = get_c(choices, states, params)

  u(c) + β * 𝔼V(a_next)
end

function Aiyagari.get_optimum(states, 𝔼V, params, endo, hh::Consumer)
  a_grid = endo.grids.a
  
  a_min, a_max = extrema(a_grid)

  res = Optim.optimize(a_next -> - objective((a_next=a_next,), states, 𝔼V, params), a_min, a_max)
  
  a_next = Optim.minimizer(res)
  val    = - Optim.minimum(res)
  conv   = Optim.converged(res)
  
  y_ = disposable_income(params.inc, params.tt, states.exo_state)
    
  pol = a_next
  pol_full = (a_next=a_next, c=get_c((a_next=a_next, ), states, params), y=y_)
  
  (pol=pol, pol_full=pol_full, val=val, conv=conv)
end

mutable struct HuggettAS{T1,T2} <: AggregateState
  r::T1
  dist::T2 # the distribution over idiosynchratic states
end

function HuggettAS(r, a_grid, z_MC)
  dist_proto = zeros((length(a_grid), length(z_MC.state_values)))
  HuggettAS(r, dist_proto)
end;

# Terminal value
function V_term_Hugg(states)
  @unpack a = states.endo_state
  a >= 0 ? zero(a) : 10_000 * a - 10_000 * one(a)
end
function markovchain(gross_inc)

  T = length(gross_inc.age_profile)
  
  trans_vec = [transition_matrix(gross_inc, i) for i in 1:T]
  grid_ = named_grid(gross_inc)

  mcT = MarkovChainT(trans_vec, grid_, 1:T)
end

exo_ss(gross_inc) = ExogenousStateSpace([markovchain(gross_inc)])