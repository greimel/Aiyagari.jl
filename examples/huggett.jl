# # Solving the Huggett model

using Revise #src
using Test #jl
using Aiyagari, DisposableIncomes
using Optim, QuantEcon, Parameters

PKG_HOME = joinpath(dirname(pathof(Aiyagari)), "..")

include(joinpath(PKG_HOME, "examples/huggett-utils.jl"))

# ## Setting up the state space

a_grid  = LinRange(-2, 5, 20)

endo  = EndogenousStateSpace((a=a_grid,))

z_grid = log.([0.5; 1.0; 1.5])
z_prob = [0.7 0.15 0.15;
          0.2 0.6 0.2;
          0.15 0.15 0.7]
z_MC = MarkovChain(z_prob, z_grid, :y)

exo = ExogenousStateSpace([z_MC])
#md ;

# ## Initialize the aggregate state and solve the model

agg_state = HuggettAS(0.05, a_grid, z_MC)

param = (β = 0.9, tt=NoGovernment(), inc=SimpleGrossIncomes(z_MC))

#using BenchmarkTools
@unpack val, policies_full = solve_bellman(endo, exo, agg_state, param, Consumer(), rtol=√eps())
#22 ms 176 itr

using DelimitedFiles #jl
#writedlm(joinpath(PKG_HOME, "test/matrices", "huggett_value.txt"), val) #src
value_test = readdlm(joinpath(PKG_HOME, "test/matrices", "huggett_value.txt")) #jl

@test all(val .≈ value_test) #jl

a_min, a_max = a_grid[[1;end]]
@test all(a_min .< policies_full.a_next .< a_max) #jl

using Plots #md
plot(a_grid, val) #md
#- #md
plot(a_grid, policies_full.a_next) #md
#- #md
plot(a_grid, policies_full.c) #md

#using BenchmarkTools #src

# let value = value, policy=policy, z_mc = z_MC #src
#   lin_ind = LinearIndices(size(value)) #src
#  #src
#   ngp_exo = length(z_mc.state_values) #src
#   n = length(policy) #src
#   len = n * ngp_exo * 2 #src
#  #src
#   I = zeros(Int, len) #src
#   J = zeros(Int, len) #src
#   V = zeros(len) #src
#  #src
#   @btime Aiyagari.controlled_markov_chain!($I, $J, $V, $lin_ind, $z_mc, #src $a_grid, $policy)
# end #src

dist = stationary_distribution(endo, exo, policies_full.next_state)
#926 μs

#writedlm("test/matrices/huggett_dist.txt", dist) #src
dist_test = readdlm(joinpath(PKG_HOME, "test/matrices", "huggett_dist.txt")) #jl
@test maximum(abs, dist .- dist_test) < 1e-8 #jl

# ## Looking at the equilibrium

function excess_demand(r)
  agg_state = HuggettAS(r, a_grid, z_MC)
  @unpack val, policies_full = solve_bellman(endo, exo, agg_state, param, Consumer(), rtol=√eps())

  dist = stationary_distribution(endo, exo, policies_full.next_state)

  sum(dist .* policies_full.a_next)
end

r_vec = 0.07:0.005:0.11 #md

plot(r_vec, excess_demand.(r_vec)) #md

# ## Finite Horizon: Solving the OLG version of the model
a_grid0 = LinRange(0, 5, 20)
endo0 = EndogenousStateSpace((a=a_grid0,))
age_grid = 25:65

paramT = (β = 0.9, tt=NoGovernment(), inc=SimpleLifecycleGrossIncomes(mc_work=z_MC, age_profile=zeros(size(age_grid))))

exoT = ExogenousStateSpace(MarkovChainT(z_prob, named_grid(z_grid, :y), age_grid))

@unpack val, policies_full = solve_bellman(endo0, exoT, agg_state, paramT, Consumer(), age_grid, V_term_Hugg)

#jl # Test terminal value
@test size(val, 3) == length(age_grid) + 1 #jl
a_named_grid0 = named_grid(endo0.grids.a, :a) #jl
a_named_endo = [(endo_state = s,) for s in a_named_grid0] #jl
@test all(val[:,:,end] .== V_term_Hugg.(a_named_endo)) #jl

#jl # Test last period
valJ = val[:,:,length(age_grid)] #jl
#writedlm("test/matrices/huggett_olg_valJ.txt", valJ) #src
valJ_test = readdlm(joinpath(PKG_HOME, "test/matrices", "huggett_olg_valJ.txt")) #jl
@test maximum(abs, valJ .- valJ_test) < 1e-14 #jl

#src TODO make olg and non-olg comparable, check if J < ∞ converges to J = +∞
#src # Test first period
#src val1 = val[:,:,1] #jl

#src val1_test = readdlm(joinpath(PKG_HOME, "test/matrices", "huggett_value.txt")) #jl
#src @test maximum(abs, val1 .- val1_test) < 1e-14 #jl

using Plots, Colors #md
#md
plt = plot(legend=false) #md
 ran = 1:3:length(age_grid) #md
 col = range(colorant"deepskyblue", stop=colorant"navyblue", length=length(ran)) #md
 map(enumerate(ran)) do (i_i,i) #md
   plot!(plt, a_grid0, val[:,:,i], color=col[i_i]) #md
 end #md
 plt #md

