# # Solving a lifecycle version the Huggett model

using Revise #src
using Test #jl
using Aiyagari, DisposableIncomes
using Optim, QuantEcon, Parameters

PKG_HOME = joinpath(dirname(pathof(Aiyagari)), "..")

include(joinpath(PKG_HOME, "examples/huggett-utils.jl"))
include(joinpath(PKG_HOME, "src/analyze.jl"))
include(joinpath(PKG_HOME, "src/visualize.jl")) #md

# ## Setting up the state space

a_grid  = LinRange(-2, 5, 20)

endo  = EndogenousStateSpace((a=a_grid,))

n_y = 13
y = tauchen(n_y, 0.91, 0.21)

y0_mc = MarkovChain(y.p, y.state_values, :y)

## # add unemployment
## πᵤ, πₑ = 0.2, 0.7
## y1_mc = add_unemployment(y0_mc, πᵤ, πₑ)

## first block of matrix
##@test all(y1_mc.p[1:n_y,1:n_y] .≈ y0_mc.p .* (1 .- πᵤ))

inc = SimpleLifecycleGrossIncomes(mc_work=y0_mc)

# ## Initialize the aggregate state and solve the model

agg_state = HuggettAS(0.09, a_grid, y)

a_grid0 = LinRange(0, 20, 70)
endo0 = EndogenousStateSpace((a=a_grid0,))

inc = SimpleLifecycleGrossIncomes(mc_work=y0_mc)

paramT = (β = 0.9,
         tt=TaxesTransfers(inc),
         inc=inc)

age_grid = 25 .+ (0:(length(paramT.inc.age_profile) + 15))

 exoT = ExogenousStateSpace(MarkovChainT(y0_mc.p, y0_mc.state_values, age_grid))

 @time (out = solve_bellman(endo0, exoT, agg_state, paramT, Consumer(), age_grid, V_term_Hugg);)

  # # Cross-sectional distribution

  π₀ = zeros((length(endo0), length(exoT)))

  dist = stationary_distributions(y0_mc)[1]

  π₀[2,:] .= dist ./ sum(dist) .* 0.3
  π₀[3,:] .= dist ./ sum(dist) .* 0.4
  π₀[4,:] .= dist ./ sum(dist) .* 0.3

  sum(π₀)

  @unpack distribution = cross_sectional_distribution(endo0, exoT, age_grid, out.policies_full.next_state, π₀)
  
  distribution_t_r = reshape(distribution, (length(endo0), length(exoT), length(age_grid)))

 plot( #md
  plt_aging_quantiles(age_grid, out.policies_full, distribution_t_r, :c), #md
  plt_aging_quantiles(age_grid, out.policies_full, distribution_t_r, :a_next), #md
  plt_aging_quantiles(age_grid, out.policies_full, distribution_t_r, :y),  #md
  legend=false #md
 ) #md

#-

using Colors #md
col = [range(colorant"deepskyblue", stop=colorant"navyblue", length=length(age_grid)), #md
       range(colorant"lightsalmon", stop=colorant"red4", length=length(age_grid)), #md
       range(colorant"yellowgreen", stop=colorant"darkgreen", length=length(age_grid))] #md

map(enumerate([1,6,13])) do (i, ii) #md
  plt_dist = plot(legend=false) #md
  for j in 1:length(age_grid) #md
    plot!(plt_dist, endo0.grids.a, distribution_t_r[:,ii,j], color=col[i][j]) #md
  end #md
  plt_dist #md
end |> x -> plot(x...)  #md





