abstract type HousingEndoStates end
abstract type TwoStates <: HousingEndoStates end

struct WealthHouse <: TwoStates end
struct XAssetHouse <: TwoStates end
struct OneState <: HousingEndoStates end

abstract type AdjustmentCosts end

struct NoAdjustmentCosts <: AdjustmentCosts end

@with_kw struct ProportionalAdjustmentCosts{T} <: AdjustmentCosts
  κ::T = 0.05 # from Berger, Guerrieri, Lorenzoni & Vavra NBER WP (=F)
end

function χ(h_prev::Number, h, p, adj::ProportionalAdjustmentCosts)
  adj.κ * h_prev * p * (h_prev != h)
end

χ(h_prev::Number, h, _, ::NoAdjustmentCosts) = zero(h)
χ(::NamedTuple, h, _, ::NoAdjustmentCosts) = zero(h)

χ(endo_state::NamedTuple, h, p, adj::AdjustmentCosts) = χ(endo_state.h, h, p, adj)

@with_kw struct Consumer{T} <: Household
  𝔼::T = Unconditional()
end

abstract type Owner <: Household end

@with_kw struct FixedHouse{T1,T2<:HousingEndoStates, T3 <: AdjustmentCosts} <: Owner
  𝔼::T1 = Unconditional()
  state::T2 = WealthHouse()
  adj::T3 = NoAdjustmentCosts()
end

@with_kw struct AdjustHouse{T1,T2<:HousingEndoStates,T3 <: AdjustmentCosts} <: Owner
  𝔼::T1 = Unconditional()
  state::T2 = OneState()
  adj::T3 = NoAdjustmentCosts()
end

Owner(args...; kwargs...) = AdjustHouse(args...; kwargs...)

@with_kw struct OwnerAdjCost{F,A,C} <: CoupledHouseholds
  keep::F = FixedHouse()
  adjust::A = AdjustHouse(state=WealthHouse(), adj=ProportionalAdjustmentCosts())
  coupling::C = DefaultCoupling()
end

households(chh::OwnerAdjCost) = [chh.keep, chh.adjust]

@with_kw struct Renter{T1,T2,T3} <: Household
  𝔼::T1 = Unconditional()
  state::T2 = OneState()
  adj::T3 = NoAdjustmentCosts()
end

@with_kw struct OwnOrRent0{A, R, C<:Coupling} <: CoupledHouseholds
  adjust::A = AdjustHouse()
  renter::R = Renter()
  coupling::C = DefaultCoupling()
end

households(chh::OwnOrRent0) = [chh.adjust, chh.renter]

@with_kw struct OwnOrRent{K, A, R, C<:Coupling} <: CoupledHouseholds
  keep::K = FixedHouse()
  adjust::A = AdjustHouse()
  renter::R = Renter()
  coupling::C = DefaultCoupling()
end

households(chh::OwnOrRent) = [chh.keep, chh.adjust, chh.renter]
