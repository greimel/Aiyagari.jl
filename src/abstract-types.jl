abstract type Household end
𝔼(hh::Household) = hh.𝔼

abstract type CoupledHouseholds end
households(chh::CoupledHouseholds) = @error "method needs to be implemented" 
Base.length(chh::CoupledHouseholds) = length(households(chh))  

households(hh::Household) = [hh]

abstract type Coupling end
struct DefaultCoupling <: Coupling end
coupling(chh::CoupledHouseholds) = chh.coupling

abstract type AggregateState end