module Aiyagari

using QuantEcon
using Interpolations
#using Optim
using Parameters
using LinearAlgebra: norm
using SparseArrays: sparse
using Arpack: eigs
using StructArrays
using ProgressMeter
using Reexport
using NamedTupleTools

include("Expectations.jl")
@reexport using .Expectations

@reexport using StateGrids

include("expectations-new.jl")
include("abstract-types.jl")
include("bellman.jl")
include("stationary-distribution.jl")
include("types.jl") 

export solve_bellman
export controlled_markov_chain, stationary_distribution, cross_sectional_distribution
export AggregateState
export Household, Owner, AdjustHouse, FixedHouse, OwnerAdjCost, Renter, Consumer, OwnOrRent0, OwnOrRent
export HousingEndoState, OneState, TwoStates, WealthHouse, XAssetHouse
export AdjustmentCosts, NoAdjustmentCosts, ProportionalAdjustmentCosts, χ
export CoupledHousehold, Coupling
export MarkovChain

examples = ["huggett", "lifecycle", "redundantstates"]

end # module
