using StatsPlots

function plt_aging_quantiles(age_grid, policies_full, dist_r, var; p=0.2:0.2:0.8,  grp_var=false)
  res = aging_quantiles(age_grid, policies_full, dist_r, var; p=p, grp_var=grp_var)
  
  n_q, n_grps, n_t = size(res)
  @assert n_t == length(age_grid)
  
  plot(title=string(var))
  for i_grp in 1:n_grps
    res_grp = @view res.q[:,i_grp,:]
    if !all(ismissing.(res_grp))
      col = i_grp #permutedims(range(colorant"Green1", stop=colorant"Green4", length=n_q))
      lab = [string(res.grp[1,i_grp,1]) fill(false, 1, length(p)-1)]
      plot!(age_grid, permutedims(res_grp), c=col, label=lab)
    end
  end
  plot!()
end

