using StatsBase
using DataFrames
using StructArrays

function aging_quantiles(age_grid, policies_full, dist_r, var; p=0.1:0.1:0.9, grp_var=false)
  pol = getproperty(policies_full, var)
  
  @assert size(pol) == size(dist_r)
  @assert size(pol, 3) == length(age_grid)
  
  if grp_var isa Symbol
    
    pol_i = getproperty(policies_full, grp_var)
    grps = unique(pol_i)
  else
    grps = [:all]
  end
  
  res = mapreduce(vcat, eachindex(age_grid)) do i_t
    if grp_var isa Symbol 
      i_grps = [pol_i[:,:,i_t] .== grp for grp in grps]
    else
      i_grps = [trues(size(pol[:,:,i_t]))]
    end
    
    mapreduce(vcat, 1:length(grps)) do i_g
      grp_inds = i_grps[i_g] 
      
      grp = grps[i_g]
      if sum(dist_r[:,:,i_t][grp_inds]) > 0
        q = quantile(vec(pol[:,:,i_t][grp_inds]), Weights(vec(dist_r[:,:,i_t][grp_inds])), p)
      else
        q = fill(missing, length(p))
      end
      T = NamedTuple{(:grp, :p, :q, :i_t),
                Tuple{typeof(grp), eltype(p), Union{Missing,Float64}, typeof(i_t)}
                      }
      map(zip(p,q)) do (p_, q_)
        T((grp, p_, q_, i_t))#(grp=grp, p=p_, q=q_, i_t=i_t) # missings screw it up! TODO
      end
    end
  end
  StructArray(reshape(res, length(p), length(grps), length(age_grid)))
end

