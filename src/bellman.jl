function get_optimum end
function get_optimum_last end

############################################################
## Helpers for pre-allocating objects
############################################################

combine_exo_endo_agg_state(exo_state, endo_state, agg_state) = (endo_state=endo_state, exo_state=merge(exo_state, ntfromstruct(agg_state)))

function proto_policy(endo, exo, i_t, value, agg_state, params, hh::Household)
  state = combine_exo_endo_agg_state(named_grid(exo, i_t)[1], grid(endo)[1], agg_state)
  𝔼V = extrapolated_𝔼V(endo, BSpline(Linear()), value, exo, i_t, 1, 𝔼(hh))
  @unpack pol, pol_full = get_optimum(state, 𝔼V, params, endo, hh)
    
  (pol_full..., next_state=pol)
end

function proto_combined_policies(proto_vec)
  n = length(proto_vec) # number of coupled value functions
  
  # which choices are present in each of the coupled problems?
  common_keys = Tuple(intersect([collect(keys(proto_vec[i])) for i in 1:n]...))
  
  tup = getproperty.(Ref(proto_vec[1]), common_keys)
  (i_v = 1, NamedTuple{common_keys}(tup)...)
end


############################################################
## Iterating the bellman operator etc
############################################################

function iterate_bellman!(value_new, value_old, policies_full, endo, exo, i_t, converged, agg_state, params, hh::Household)
  n = length(value_new)
  prog = Progress(n, desc="Iterating", offset=1, dt=1)
  for (i_exo, exo_state) in enumerate(named_grid(exo, i_t))
    # Create interpolated expected value function
    
    # TODO this needs to be handled in a cleaner way
    if hasproperty(params, :itp_scheme)
      itp_scheme = params.itp_scheme
    else
      #itp_scheme = BSpline(Linear())
      itp_scheme = BSpline(Cubic(Line(OnGrid())))
    end
        
    𝔼V = extrapolated_𝔼V(endo, itp_scheme, value_old, exo, i_t, i_exo, 𝔼(hh))
    
    Threads.@threads for i_endo in 1:length(endo)
      endo_state = endo.grid[i_endo]
    #for (i_endo, endo_state) in enumerate(endo.grid)

      states = combine_exo_endo_agg_state(exo_state, endo_state, agg_state) 
      ProgressMeter.next!(prog)
      @unpack pol, pol_full, val, conv = get_optimum(states, 𝔼V, params, endo, hh)

      #print("b ")
      
      policies_full[i_endo, i_exo] = (pol_full..., next_state=pol)
      value_new[i_endo, i_exo] = val 
      #converged[i_endo, i_exo] = conv 
    end
  end
  #non_finite_values = 1 - mean(isfinite.(value_new))
  #if non_finite_values < 1
  #    @warn "i_t: $i_t, fraction of non-finite values: $non_finite_values"
  #end
end

############################################################
## Finite Horizon (OLG)
############################################################

function solve_bellman(endo, exo, aggregate_state, params, hh::Household, t_grid, V_term)
  containers = initialize_values_policies(endo, exo, aggregate_state, params, hh, t_grid)
  @unpack value, policies_full, converged = containers
  
  solve_bellman_T!(value, policies_full, endo, exo, converged, aggregate_state, params, hh, t_grid, V_term)
      
  (val = value, policies_full=StructArray(policies_full), converged=converged)
end

function initialize_values_policies(endo, exo, aggregate_state, params, hh::Household, t_grid)
  container_size = (length(endo), length(exo), length(t_grid))

  value = zeros((length(endo), length(exo), length(t_grid)+1))
  
  proto_pol_full = proto_policy(endo, exo, 1, value[:,:,1], aggregate_state, params, hh)
  
  policies_full = fill(proto_pol_full, container_size)
  converged = trues(container_size)
  
  (value=value, policies_full=policies_full, converged=converged)
end

function solve_bellman_T!(value, policies_full, endo, exo, converged, aggregate_state, params, hh::Household, t_grid, V_term)
  
  # Solving the last period (special-cased for bequests, etc)
  terminal_value!(@view(value[:,:,end]), endo, exo, aggregate_state, length(t_grid), V_term)
  
  if !all(isfinite.(@view(value[:,:,end])))
    throw(ArgumentError("V_term returned non-finite value. Check V_term or statespace."))
  end
  
  # Solving all other periods
  @showprogress "Bellman(T): VFI" for i_t in reverse(1:length(t_grid))
    iterate_bellman!(@view(value[:,:,i_t]),
                     @view(value[:,:,i_t+1]),
                     @view(policies_full[:,:,i_t]),
                     endo, exo, i_t,
                     @view(converged[:,:,i_t]), aggregate_state, params, hh)
  end
  
  number_conv = sum(converged)
  
  length(converged) == number_conv || @warn "Bellman didn't converge at $(round((1-number_conv / length(converged)) * 100, digits=4))% ($(length(converged) - number_conv) states)"
end

function terminal_value!(value, endo, exo, agg_state, i_T, V_term::Function)
  for (i_exo, exo_state) in enumerate(named_grid(exo, i_T))
    for (i_endo, endo_state) in enumerate(grid(endo)) 
      states = combine_exo_endo_agg_state(exo_state, endo_state, agg_state)
      value[i_endo, i_exo] = V_term(states)
    end
  end
end

############################################################
## Infinite horizon
############################################################

function solve_bellman(endo, exo, aggregate_state, params, hh::Household; maxiter=200, rtol=eps()^0.4)
  containers = initialize_values_policies(endo, exo, aggregate_state, params, hh)
  @unpack value_old, value_new, policies_full, converged = containers
  
  solve_bellman!(value_old, value_new, policies_full, endo, exo, converged, aggregate_state, params, hh::Household; maxiter=maxiter, rtol=rtol)
    
  (val = value_new, policies_full=StructArray(policies_full), converged=converged)
end

function initialize_values_policies(endo, exo, aggregate_state, params, hh::Household)
  container_size = (length(endo), length(exo))

  value_old = zeros(container_size)
  value_new = zeros(container_size)
  
  proto_pol_full = proto_policy(endo, exo, 0, value_new, aggregate_state, params, hh)
  
  policies_full = fill(proto_pol_full, container_size)
  converged = trues(container_size)
  
  (value_old=value_old, value_new=value_new, policies_full=policies_full, converged=converged)
end

function solve_bellman!(value_old, value_new, policies_full, endo, exo, converged, aggregate_state, params, hh::Household; maxiter, rtol)
  
  prog = ProgressThresh(rtol, "Bellman: VFI")
  for i in 1:maxiter
    iterate_bellman!(value_new, value_old, policies_full, endo, exo, 0, converged, aggregate_state, params, hh)
    diff = norm(value_old - value_new)
    
    adj_fact = max(norm(value_old), norm(value_new))
     
    ProgressMeter.update!(prog, diff / adj_fact)
    value_old .= value_new
    
    if diff < rtol * adj_fact
      break
    end
    if i == maxiter
      print("\n"^2)
      @warn "reached $maxiter, diff= $diff"
    end
  end
  number_conv = sum(converged)
  
  length(converged) == number_conv || @warn "Bellman didn't converge at $(round((1-number_conv / length(converged)) * 100, digits=4))% ($(length(converged) - number_conv) states)"

end

############################################################
## Coupled value functions : Infinite Horizon
############################################################

function solve_bellman(endo, exo, aggregate_state, params, chh::CoupledHouseholds; maxiter=200, rtol=eps()^0.4)
  containers = initialize_values_policies(endo, exo, aggregate_state, params, chh)
  @unpack W_old, W_new, V, policies_full, policy_hh, converged = containers
  
  solve_bellman!(W_old, W_new, V, policies_full, policy_hh, endo, exo, converged, aggregate_state, params, chh; maxiter=maxiter, rtol=rtol)
  
  policies_full_c = combine_policies(policies_full, policy_hh) |> StructArray
  
  (val = W_new, policy_hh=policy_hh, policies_full=policies_full_c, converged=converged)
end

function initialize_values_policies(endo, exo, aggregate_state, params, chh::CoupledHouseholds)
  container_size = (length(endo), length(exo))
  
  hh_tup = households(chh)
  n = length(chh)
  
  V         = [zeros(container_size) for i in 1:n]
  W_old     = zeros(container_size)
  W_new     = zeros(container_size)
  converged = [trues(container_size) for i in 1:n]
  
  policy_hh = zeros(Int, container_size)
  
    
  # @unpack proto_pol, proto_pol_full
  proto = map(1:n) do i
    proto_policy(endo, exo, 0, V[1], aggregate_state, params[i], hh_tup[i])
  end
    
  policies_full = fill.(proto, Ref(container_size))
  
  (W_old=W_old, W_new=W_new, V=V, policies_full=policies_full, policy_hh=policy_hh, converged=converged)
end

function solve_bellman!(W_old, W_new, V::Vector, policies_full::Vector, policies_hh, endo, exo, converged::Vector, aggregate_state, params::Vector, chh::CoupledHouseholds; maxiter, rtol)
    
  prog = ProgressThresh(rtol, "Solving Bellman equation")

  for i in 1:maxiter
    iterate_bellman!.(V, Ref(W_old), policies_full, Ref(endo), Ref(exo), 0, converged, Ref(aggregate_state), params, households(chh))
    
    update_coupled_values!(W_new, V, policies_hh, coupling(chh))
    
    diff = norm(W_old - W_new)
    
    adj_fact = max(norm(W_old), norm(W_new))
    
    relative_error = diff / adj_fact
    
    ProgressMeter.update!(prog, relative_error)
    
    W_old .= W_new
    
    if relative_error < rtol
      break
    end

    if i == maxiter
      print("\n"^2)
      @warn "reached $maxiter, diff= $relative_error"
    end
  end
  
  all(all.(converged)) || @warn "optimization didn't converge at $((1 .- mean.(converged)) .* 100)%"

end

function update_coupled_values!(W, V::Vector, policies_hh, ::DefaultCoupling)
  for i in eachindex(W)
    W[i], policies_hh[i]  = findmax([V_j[i] for V_j in V])
  end 

  nothing
end

function combine_policies(policies_vec, policy_hh)
  n = length(policies_vec) # number of coupled value functions
  
  # which choices are present in each of the coupled problems?
  common_keys = Tuple(intersect([collect(keys(policies_vec[i][1])) for i in 1:n]...))
  i = 1 # TODO generalize for case where V₁ ≠ V₂
  joint_policy = map(eachindex(policies_vec[i])) do j
    # which of the coupled value functions is chosen?
    i_v = policy_hh[j]
    # select only the common policies and save them as a NamedTuple
    vals = getindex.(Ref(policies_vec[i_v][j]), common_keys)
    pol0 = NamedTuple{common_keys}(vals)
    # combine into a NamedTuple 
    (i_v = i_v, pol0...)
  end |> x -> reshape(x, size(policies_vec[1])) #|> StructArray
end

############################################################
## Coupled value functions : Finite Horizon
############################################################

function solve_bellman(endo, exo, aggregate_state, params, chh::CoupledHouseholds, t_grid, V_term)
  containers = initialize_values_policies(endo, exo, aggregate_state, params, chh, t_grid)
  
  @unpack W, V, policies_full, policies_full_vec, policy_hh, converged = containers
  
  solve_bellman_T!(W, V, policies_full, policies_full_vec, policy_hh, endo, exo, aggregate_state, params, chh, t_grid, V_term)
  
  #policies_full_c = combine_policies(policies_full, policy_hh)
  
  (W=W, policy_hh=policy_hh, policies_full = StructArray(policies_full), policies_full_vec=policies_full_vec)

end

function initialize_values_policies(endo, exo, aggregate_state, params, chh::CoupledHouseholds, t_grid)
  
  container_size0 = (length(endo), length(exo))
  container_size = (length(endo), length(exo), length(t_grid))
  
  hh_tup = households(chh)
  n = length(chh)
  
  V        = [zeros(container_size0) for i in 1:n]
  W        = zeros((length(endo), length(exo), length(t_grid)+1))

  converged = [trues(container_size) for i in 1:n]
  
  policy_hh = zeros(Int, container_size)
  
    
  # @unpack proto_pol, proto_pol_full
  proto_vec = map(1:n) do i
    proto_policy(endo, exo, 1, V[1], aggregate_state, params[i], hh_tup[i])
  end
  proto_combined = proto_combined_policies(proto_vec)
  
  policies_full_vec = [fill.(proto_vec, Ref(container_size0)) for i in t_grid]
  
  policies_full = fill(proto_combined, container_size) 
  
  (W=W, V=V, policies_full=policies_full, policies_full_vec=policies_full_vec, policy_hh=policy_hh, converged=converged)
end

function solve_bellman_T!(W, V, policies_full, policies_full_vec, policies_hh, endo, exo, aggregate_state, params, chh, t_grid, V_term)
    
  # Solving the last period (special-cased for bequests, etc)
  terminal_value!(@view(W[:,:,end]), endo, exo, aggregate_state, length(t_grid), V_term) 
  
  @showprogress "Bellman(T): VFI" for i_t in reverse(1:length(t_grid))
    iterate_bellman!.(V,
                      Ref(@view(W[:,:,i_t+1])),
                      policies_full_vec[i_t],
                      Ref(endo), Ref(exo), i_t,
                      missing, #@view(converged[:,:,i_t]),
                      Ref(aggregate_state), params, households(chh))

    update_coupled_values!(@view(W[:,:,i_t]), V, @view(policies_hh[:,:,i_t]), coupling(chh))
    

    policies_full[:,:,i_t] .= combine_policies(policies_full_vec[i_t],
                     @view(policies_hh[:,:,i_t])
                     )
  end
end

